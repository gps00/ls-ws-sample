;(function() {

    var base = MODULE_PATH + '/js/';

    AUI().applyConfig(
        {
            groups: {
                mymodulesoverride: { //mymodulesoverride
                    base: base,
                    combine: Liferay.AUI.getCombine(),
                    filter: Liferay.AUI.getFilterConfig(),
                    modules: {
                        'liferay-session-override': { //my-module-override
                            path: 'session-override.js', //my-module.js
                            condition: {
                                name: 'liferay-session-override', //my-module-override
                                trigger: 'liferay-session', //original module
                                when: 'instead'
                            }
                        }
                    },
                    root: base
                }
            }
        }
    );
})();