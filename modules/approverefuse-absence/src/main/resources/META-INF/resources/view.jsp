<%@ include file="/init.jsp" %>
<liferay-portlet:resourceURL var="getData" id="get">
</liferay-portlet:resourceURL>

<liferay-portlet:resourceURL var="sendData" id="send">
</liferay-portlet:resourceURL>
<% 
   String encId=null;
   String fullName=null;
   if(request.getAttribute("encId")!=null){
	   encId=request.getAttribute("encId").toString();
       long uid =UserLocalServiceUtil.getUserIdByScreenName(themeDisplay.getCompanyId(),encId);
	   User user2 = UserLocalServiceUtil.getUserById(uid);
	   fullName=user2.getFullName();
   }
%>
<script type="text/javascript">
var lang = "<%= themeDisplay.getLocale()%>";
var encId="<%=encId%>";
var fullName="<%=fullName%>";
console.log(encId);
var app=angular.module("view", [ 'ngAnimate', 'ui.bootstrap',
	'angularUtils.directives.dirPagination', 'wt.responsive']);
	
app.controller("viewController",["$scope","$uibModal", "$http",function($scope, $uibModal, $http){
	$scope.loading=true;
	$scope.lang = lang;
	$scope.encId=encId;
	$scope.error = false;
	$scope.msg=false;
	$scope.employees=[];
	var data ={
			encId:$scope.encId
		};
	$http.post('<%=getData %>', JSON.stringify(data))
    .then(function (response) {
    	console.log(response.data);
    	$scope.employees=response.data;
    	if($scope.employees[0].firstName == undefined){  		
    		$scope.msg=true;
    	}  
    	$scope.loading=false;
    },function(data) {
    	console.log(data);
    	$scope.loading=false;
    	$scope.error = true;
    });
	$scope.action=function(data){		
		$scope.action_data=data;
		var modalInstance = $uibModal.open({
			animation : true,
			backdrop: 'static',
			keyboard: false,
			windowTopClass: 'action-modal',
			ariaLabelledBy : 'modal-title',
			ariaDescribedBy : 'modal-body',
			templateUrl : 'action.html',
			controller : 'ModalInstanceAction',
			controllerAs : '$actionCtrl',
			scope : $scope
		});
		modalInstance.result.then(function() {

		}, function() {

		});
	};
	$scope.approve=function(appData){
		$scope.appData=appData;
		console.log($scope.appData);
		var data={
				empId:$scope.appData.emplID,
				pin_name: $scope.appData.pinNm,
				start_date: $scope.appData.beginDate,
				end_date: $scope.appData.endDate,
				status_flag: "APV",
				comments: $scope.appData.comment
		};
		console.log(data);
		$scope.loading=true;
		$http.post('<%=sendData %>', JSON.stringify(data))
		.then(function (response) {
			console.log(response.data);
			if(response.status == 200){
				$scope.loading=false;
				var modalInstance = $uibModal.open({
					animation : true,
					backdrop: 'static',
					keyboard: false,
					windowTopClass: 'approve-modal',
					ariaLabelledBy : 'modal-title',
					ariaDescribedBy : 'modal-body',
					templateUrl : 'approve.html',
					controller : 'ModalInstanceApprove',
					controllerAs : '$approveCtrl',
					scope : $scope
				});
				modalInstance.result.then(function() {

				}, function() {

				});
			}
		},function(data) {
			$scope.loading=false;
			console.log(data.status);
			var modalInstance = $uibModal.open({
				animation : true,
				backdrop: 'static',
				keyboard: false,
				windowTopClass: 'error-modal',
				ariaLabelledBy : 'modal-title',
				ariaDescribedBy : 'modal-body',
				templateUrl : 'error.html',
				controller : 'ModalInstanceError',
				controllerAs : '$errorCtrl',
				scope : $scope
			});
			modalInstance.result.then(function() {

			}, function() {

			});
		});	
		
	};
	$scope.refuse=function(refData){
		$scope.refData=refData;
		console.log($scope.refData);
		var data={
				empId:$scope.refData.emplID,
				pin_name: $scope.refData.pinNm,
				start_date: $scope.refData.beginDate,
				end_date: $scope.refData.endDate,
				status_flag: "DNY",
				comments: $scope.refData.comment
		};
		$scope.loading=true;
		$http.post('<%=sendData %>', JSON.stringify(data))
		.then(function (response) {
			console.log(response);
			if(response.status == 200){
				$scope.loading=false;
				var modalInstance = $uibModal.open({
					animation : true,
					backdrop: 'static',
					keyboard: false,
					windowTopClass: 'refuse-modal',
					ariaLabelledBy : 'modal-title',
					ariaDescribedBy : 'modal-body',
					templateUrl : 'refuse.html',
					controller : 'ModalInstanceRefuse',
					controllerAs : '$refuseCtrl',
					scope : $scope
				});
				modalInstance.result.then(function() {

				}, function() {

				});
			}
		},function(data) {
			$scope.loading=false;
			console.log(data.status);
			var modalInstance = $uibModal.open({
				animation : true,
				backdrop: 'static',
				keyboard: false,
				windowTopClass: 'error-modal',
				ariaLabelledBy : 'modal-title',
				ariaDescribedBy : 'modal-body',
				templateUrl : 'error.html',
				controller : 'ModalInstanceError',
				controllerAs : '$errorCtrl',
				scope : $scope
			});
			modalInstance.result.then(function() {

			}, function() {

			});
		});	
	};
	$scope.startDate=function(emp){
		if(emp.firstPart == "First Half"){
			return "icon-adjust icon-rotate-180";
		}else if(emp.firstPart == "Second Half"){
			return "icon-adjust";
		}else if(emp.startDayFrom != ""){
			return "icon-time";
		}else{
		    return "icon-circle";
		}
	};
$scope.endDate=function(emp){
	if(emp.beginDate == emp.endDate){
	   if(emp.firstPart == "First Half"){
		  return "icon-adjust icon-rotate-180";
	   }else if(emp.firstPart == "Second Half"){
		  return "icon-adjust";
	   }else if(emp.startDayFrom != ""){
		  return "icon-time";
	   }else{
	      return "icon-circle";
	   }
	}else{
		if(emp.secondPart == "First Half"){
			return "icon-adjust icon-rotate-180";
		}else if(emp.secondPart == "Second Half"){
			return "icon-adjust";
		}else if(emp.endDayFrom != ""){
			return "icon-time";
		}else{
		    return "icon-circle";
		}
	}
    };
}]);	
	app.controller('ModalInstanceAction', [ '$scope', '$uibModalInstance',
		function($scope, $uibModalInstance) {
		var $actionCtrl = this;
		$actionCtrl.lang = $scope.lang;
		$actionCtrl.action_data=$scope.action_data;
		$actionCtrl.action_data.comment="";
		console.log($actionCtrl.action_data);
		$actionCtrl.cancel = function() {
			$uibModalInstance.dismiss('cancel');
		};
}]);
	app.controller('ModalInstanceApprove', [ '$scope', '$uibModalInstance',
		function($scope, $uibModalInstance) {
		var $approveCtrl = this;
		
		$approveCtrl.cancel = function() {
			$uibModalInstance.dismiss('cancel');
		};
}]);
	app.controller('ModalInstanceRefuse', [ '$scope', '$uibModalInstance',
		function($scope, $uibModalInstance) {
		var $refuseCtrl = this;
		
		$refuseCtrl.cancel = function() {
			$uibModalInstance.dismiss('cancel');
		};
}]);
	app.controller('ModalInstanceError', [ '$scope', '$uibModalInstance',
		function($scope, $uibModalInstance) {
		var $errorCtrl = this;
		
		$errorCtrl.cancel = function() {
			$uibModalInstance.dismiss('cancel');
		};
}]);
</script>
<div ng-app="view" ng-controller="viewController as ctrl" class="approve-refuse ng-cloak">
    <h2><liferay-ui:message key="app.heading"/></h2>
    <!-- Loader  -->
      <div ng-cloak ng-show="loading" id="overlay-approve">
         <div class="loader"></div>
         <img class="loadingImage" src="<%= request.getContextPath()%>/images/loading.gif" />
      </div>
      <!-- Loader  -->
	<table wt-responsive-table class="table">
		<thead>
			<tr>
				<th></th>
				<th><liferay-ui:message key="app.abs.desc"/></th>
				<th><liferay-ui:message key="app.abs.bDate"/></th>
				<th><liferay-ui:message key="app.abs.eDate"/></th>
				<th><liferay-ui:message key="app.abs.dept"/></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<tr dir-paginate="emp in employees| itemsPerPage: 10" pagination-id="index">
				<td data-title="" ng-class="{'green' : emp.color=='0' , 'yellow' : emp.color=='1', 'red' : emp.color=='2', 'none' : emp.color=='4'}">
					<div class="row m-0">
						<div class="col-sm-3 col-xs-2 visible-md visible-lg p-0">
							<img ng-if="emp.imageUrl == null" class="emp-img" src="/image/user_male_portrait.png">
                            <img ng-if="emp.imageUrl != null" class="emp-img" src="{{emp.imageUrl}}">
						</div>
						<div class="col-sm-7 col-xs-8 p-0">
							<p class="pt-2">{{emp.firstName}} {{emp.lastName}}</p>
							<p class="text-warning">{{emp.emplID}}</p>
						</div>
					</div>
				</td>
				<td data-title="<liferay-ui:message key="app.abs.desc"/>" ng-if="lang=='nl_NL'">{{emp.absenceDescriptionNL}}</td>
				<td data-title="<liferay-ui:message key="app.abs.desc"/>" ng-if="lang=='fr_FR'">{{emp.absenceDescriptionFR}}</td>
				<td data-title="<liferay-ui:message key="app.abs.bDate"/>">{{emp.beginDate}}<i ng-class="startDate(emp)" class="pl-1"></i></td>
				<td data-title="<liferay-ui:message key="app.abs.eDate"/>">{{emp.endDate}}<i ng-class="endDate(emp)" class="pl-1"></i></td>
				<td data-title="<liferay-ui:message key="app.abs.dept"/>" ng-if="lang=='nl_NL'">{{emp.departmentDescriptionNL}}</td>
				<td data-title="<liferay-ui:message key="app.abs.dept"/>" ng-if="lang=='fr_FR'">{{emp.departmentDescriptionFR}}</td>
				<td data-title=""><button class="btn btn-sm btn-info mb-1"
						ng-click="action(emp)"><liferay-ui:message key="app.abs.action"/></button></td>
			</tr>
		</tbody>
	</table>
	<dir-pagination-controls max-size="6" boundary-links="true" template-url="name-pagination.tpl.html" pagination-id="index"></dir-pagination-controls>
    <p ng-show="error" style="color: red;text-align: center;"><liferay-ui:message key="app.abs.error"/></p>
    <p ng-show="msg" style="color: red;text-align: center;"><liferay-ui:message key="app.abs.msg"/></p>
<script type="text/ng-template" id="name-pagination.tpl.html">
<div>
<ul class="pagination pull-right" ng-if="1 < pages.length || !autoHide">
    <li ng-if="boundaryLinks" ng-class="{ disabled : pagination.current == 1 }">
        <a href="" ng-click="setCurrent(1)">&laquo;</a>
    </li>
    <li ng-if="directionLinks" ng-class="{ disabled : pagination.current == 1 }">
        <a href="" ng-click="setCurrent(pagination.current - 1)">&lsaquo;</a>
    </li>
    <li ng-repeat="pageNumber in pages track by tracker(pageNumber, $index)" ng-class="{ active : pagination.current == pageNumber, disabled : pageNumber == '...' }">
        <a href="" ng-click="setCurrent(pageNumber)">{{ pageNumber }}</a>
    </li>

    <li ng-if="directionLinks" ng-class="{ disabled : pagination.current == pagination.last }">
        <a href="" ng-click="setCurrent(pagination.current + 1)">&rsaquo;</a>
    </li>
    <li ng-if="boundaryLinks"  ng-class="{ disabled : pagination.current == pagination.last }">
        <a href="" ng-click="setCurrent(pagination.last)">&raquo;</a>
    </li>
</ul>
</div>
</script>
<script type="text/ng-template" id="action.html">
       <div class="modal-header">
            <h3 class="modal-title"></h3>
            <a ng-click="$actionCtrl.cancel()"><i class="icon-remove" aria-hidden="true"></i></a>
        </div>
        <div class="modal-body" id="modal-body">
        <div class="row">
        <div class="col-md-12 p-0">
         <div class="row m-0 pt-2 pb-2">
                  <div class="col-md-3 p-0">
                        <div class="row m-0">
						<div class="col-sm-3 col-xs-2 visible-md visible-lg p-0">
							<img ng-if="emp.imageUrl == null" class="emp-img" src="/image/user_male_portrait.png">
                            <img ng-if="emp.imageUrl != null" class="emp-img" src="{{$actionCtrl.action_data.imageUrl}}">
						</div>
						<div class="col-sm-7 col-xs-8 p-0">
							<p class="pt-2">{{$actionCtrl.action_data.firstName}} {{$actionCtrl.action_data.lastName}}</p>
							<p class="text-warning">{{$actionCtrl.action_data.emplID}}</p>
						</div>
                        </div>
                  </div>
                  <div class="col-md-7 p-0">
                    <div class="row">
                     <div class="col-md-12 p-2">
                      <p><liferay-ui:message key="app.abs.lDesc"/>:</p>
                      <p class="font-weight-bold pb-1 pt-1" ng-if="$actionCtrl.lang=='nl_NL'">{{$actionCtrl.action_data.absenceDescriptionNL}}</p>
                      <p class="font-weight-bold pb-1 pt-1" ng-if="$actionCtrl.lang=='fr_FR'">{{$actionCtrl.action_data.absenceDescriptionFR}}</p>
                     </div>
                    </div>
                    <div class="row p-2">
                    <div class="col-md-3 col-sm-6 col-xs-6 p-0">
                    <p><liferay-ui:message key="app.abs.sDate"/>:</p>
                    <p class="font-weight-bold pb-1 pt-1">{{$actionCtrl.action_data.submittedDate}}</p>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6 p-0">
                    <p><liferay-ui:message key="app.abs.bDate"/>:</p>
                    <p class="font-weight-bold pb-1 pt-1"><i ng-class="startDate($actionCtrl.action_data)" class="pr-1"></i>{{$actionCtrl.action_data.beginDate}}</p>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6 p-0">
                    <p><liferay-ui:message key="app.abs.eDate"/>:</p>
                    <p class="font-weight-bold pb-1 pt-1"><i ng-class="endDate($actionCtrl.action_data)" class="pr-1"></i>{{$actionCtrl.action_data.endDate}}</p>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6 p-0">
                    <p ng-if="$actionCtrl.action_data.color && $actionCtrl.action_data.color!=='4'"><liferay-ui:message key="app.abs.prob"/>:</p>
                    <p class="font-weight-bold pb-1 pt-1" ng-if="$actionCtrl.action_data.color==='0'" style="color:#8CC152;"><liferay-ui:message key="app.abs.0"/></p>
                    <p class="font-weight-bold pb-1 pt-1" ng-if="$actionCtrl.action_data.color==='1'" style="color:#FFD855;"><liferay-ui:message key="app.abs.1"/></p>
                    <p class="font-weight-bold pb-1 pt-1" ng-if="$actionCtrl.action_data.color==='2'" style="color:#ef2637;"><liferay-ui:message key="app.abs.2"/></p>
                    </div>
                    </div>
                  </div>
		</div>
         <textarea ng-model="$actionCtrl.action_data.comment" class="w-100 mt-2 p-2" rows="5" placeholder="<liferay-ui:message key="app.abs.comment"/>"></textarea>      
        </div>
        </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-sm btn-white" type="button" ng-click="$actionCtrl.cancel();approve($actionCtrl.action_data)">Approve</button>
            <button class="btn btn-sm btn-white" type="button" ng-click="$actionCtrl.cancel();refuse($actionCtrl.action_data)">Deny</button>
        </div>
</script>
	<script type="text/ng-template" id="approve.html">

        <div class="modal-body" id="modal-body">  
        <div class="row">
        <div class="col-md-12 text-center">
         <b><liferay-ui:message key="app.abs.success"/></b>
        </div>
        </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-sm btn-white" type="button" ng-click="$approveCtrl.cancel()">OK</button>            
        </div>
</script>
<script type="text/ng-template" id="error.html">

        <div class="modal-body" id="modal-body">  
        <div class="row">
        <div class="col-md-12 text-center">
         <b><liferay-ui:message key="app.abs.error"/></b>
        </div>
        </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-sm btn-white" type="button" ng-click="$errorCtrl.cancel()">OK</button>            
        </div>
</script>
<script type="text/ng-template" id="refuse.html">

        <div class="modal-body" id="modal-body">  
        <div class="row">
        <div class="col-md-12 text-center">
         <b>Refusal</b>
        </div>
        </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-sm btn-white" type="button" ng-click="$refuseCtrl.cancel()">OK</button>            
        </div>
</script>
</div>