package be.bpost.approverefuse.absence.util;

public class ApiHeaders {
	

	private String xApikey;
	private String apiGatewayUrl;
	private String domainName;
	private String secretKey;
	
	public ApiHeaders(String xApiKey, String apiGatewayUrl,String domainName, String secretKey ) {
		super();
		// TODO Auto-generated constructor stub
		this.xApikey=xApiKey;
		this.apiGatewayUrl=apiGatewayUrl;
		this.domainName=domainName;
		this.secretKey=secretKey;
	}

	public String getxApikey() {
		return xApikey;
	}

	public void setxApikey(String xApikey) {
		this.xApikey = xApikey;
	}

	public String getApiGatewayUrl() {
		return apiGatewayUrl;
	}

	public void setApiGatewayUrl(String apiGatewayUrl) {
		this.apiGatewayUrl = apiGatewayUrl;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

}
