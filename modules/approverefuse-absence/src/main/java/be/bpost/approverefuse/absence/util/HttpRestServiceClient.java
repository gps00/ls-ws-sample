package be.bpost.approverefuse.absence.util;

import java.io.IOException;


import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import be.bpost.approverefuse.absence.bean.ServiceRequestParamBean;



public class HttpRestServiceClient {
	
	private static final Log LOGGER = LogFactoryUtil.getLog(HttpRestServiceClient.class);

	public JSONObject getServiceResponse(ServiceRequestParamBean serviceRequestParamBean) throws ClientProtocolException, IOException {
		JSONObject responseObj=JSONFactoryUtil.createJSONObject();
		LOGGER.info("ApproverefuseAbsence Rest client: ReqID: " + serviceRequestParamBean.getReqId() + " : RequestType: "
				+ serviceRequestParamBean.getRequestType() + " : RequestedFor: "
				+serviceRequestParamBean.getEmpId()+" : RequestedBy: "
				+ serviceRequestParamBean.getRequestedBy() + " :  env: " + serviceRequestParamBean.getEnv()
				+ " : apiPath : " + serviceRequestParamBean.getApiPath() + " : Body : " + serviceRequestParamBean.getBodyEntity());
		ApiHeaders apiHeader=ApiGatewayMapping.getApiHeaders(serviceRequestParamBean.getEnv());
		/*JWT token implementation*/
		CreateJwtToken jwtToken=new CreateJwtToken();
		String token = jwtToken.createJwtToken(serviceRequestParamBean.getReqId(), serviceRequestParamBean.getEmpId(),
				apiHeader.getSecretKey(), apiHeader.getDomainName());
		if(null!=token){
			CloseableHttpClient client = HttpClients.createDefault();
		    HttpPost httpPost = new HttpPost(apiHeader.getApiGatewayUrl()+serviceRequestParamBean.getApiPath());
		    StringEntity entity = new StringEntity(serviceRequestParamBean.getBodyEntity());
		    httpPost.setEntity(entity);
		    httpPost.setHeader("Content-type", "application/json");
		    httpPost.setHeader("x-api-key",apiHeader.getxApikey());
		    httpPost.setHeader("domain",apiHeader.getDomainName());
		    httpPost.setHeader("authorizationtoken",token);	 
		    CloseableHttpResponse response = client.execute(httpPost);
		    String responseBody = EntityUtils.toString(response.getEntity());   
		   // if(!serviceRequestParamBean.getApiPath().equalsIgnoreCase("bpovoidabsence"))
		    responseObj.put("statusCode",response.getStatusLine().getStatusCode());
		    responseObj.put("response",responseBody);
		    LOGGER.info("ApproverefuseAbsence  Rest client: response body "+responseBody);
		    client.close();
			return responseObj;
		}
		else{
			LOGGER.info("ApproverefuseAbsence Rest client: Issue creating token ");
			return null;
		}
	}
}
