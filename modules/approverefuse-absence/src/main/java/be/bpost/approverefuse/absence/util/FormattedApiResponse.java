package be.bpost.approverefuse.absence.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;

public class FormattedApiResponse {
	private static final Log LOGGER = LogFactoryUtil.getLog(FormattedApiResponse.class);
	public String getEnv(String enviorment) {
		if(enviorment.contains("bpost-liferay-bpo1801-tst-liferay.apps.c2151-de.firelay.io") ||enviorment.contains("localhost")){
			return "DEV";
		}else if (enviorment.contains("bpost-bpo1801-acc-webserver.apps.c2151-de.firelay.io")) {
			return "UAT";
		}
		else if(enviorment.contains("bpost4me.be")){
			return "PROD";
		}else{
			return null;
		}
		
	}
	
	public JSONArray getApproveRefuseAbsListDetailsApiJsonResponse(JSONArray approveRefuseAbsListDetailsArray,String reqId, long companyID,
			ThemeDisplay themeDisplay){
		 LOGGER.info("getApproveRefuseAbsListDetailsApiJsonResponse responseObj :"+approveRefuseAbsListDetailsArray);
		   JSONArray employeesAbsenceCancelDetailList=new JSONArray();
		   Utility utility = new Utility();
		   try{
		   
		   for(int i=0; i<approveRefuseAbsListDetailsArray.length(); i++) {
				     JSONObject employeeAppRefDet = new JSONObject();
				     JSONObject tempEmployeeAppRefDet = new JSONObject();
				     employeeAppRefDet = approveRefuseAbsListDetailsArray.getJSONObject(i);
				     tempEmployeeAppRefDet.put("absenceDescriptionNL",employeeAppRefDet.getString("Absence_description_NL"));
				     tempEmployeeAppRefDet.put("absenceDescriptionFR",employeeAppRefDet.getString("Absence_description_FR"));
				     tempEmployeeAppRefDet.put("startDayTo",employeeAppRefDet.getString("Start_Day_To"));
				     tempEmployeeAppRefDet.put("jobcodDescriptionNL",employeeAppRefDet.getString("Jobcode_description_NL"));
				     tempEmployeeAppRefDet.put("jobcodDescriptionFR",employeeAppRefDet.getString("Jobcode_description_FR"));
				     tempEmployeeAppRefDet.put("departmentDescriptionNL",employeeAppRefDet.getString("Department_description_NL"));
				     tempEmployeeAppRefDet.put("departmentDescriptionFR",employeeAppRefDet.getString("Department_description_FR"));
				     tempEmployeeAppRefDet.put("firstName",employeeAppRefDet.getString("FirstName"));
				     tempEmployeeAppRefDet.put("color",employeeAppRefDet.getString("Color"));
				     tempEmployeeAppRefDet.put("firstPart",employeeAppRefDet.getString("First_Part"));
				     tempEmployeeAppRefDet.put("emplID",employeeAppRefDet.getString("EmplID"));
				     tempEmployeeAppRefDet.put("endDayFrom",employeeAppRefDet.getString("End_Day_From"));
				     tempEmployeeAppRefDet.put("beginDate",employeeAppRefDet.getString("Begin_date"));
				     tempEmployeeAppRefDet.put("absenceReason",employeeAppRefDet.getString("Absence_Reason"));
				     tempEmployeeAppRefDet.put("endDayTo",employeeAppRefDet.getString("End_Day_To"));
				     tempEmployeeAppRefDet.put("startDayFrom",employeeAppRefDet.getString("Start_Day_From"));
				     tempEmployeeAppRefDet.put("secondPart",employeeAppRefDet.getString("Second_Part"));
				     tempEmployeeAppRefDet.put("pinNm",employeeAppRefDet.getString("Pin_Nm"));
				     tempEmployeeAppRefDet.put("submittedDate",employeeAppRefDet.getString("Submitted_Date"));
				     tempEmployeeAppRefDet.put("lastName",employeeAppRefDet.getString("LastName"));
				     tempEmployeeAppRefDet.put("requestorComments",employeeAppRefDet.getString("Requestor_Comments"));
				     tempEmployeeAppRefDet.put("endDate",employeeAppRefDet.getString("End_date"));	    
				     tempEmployeeAppRefDet.put("imageUrl", utility.retrieveUserImageUrl(reqId, companyID,
				    		 employeeAppRefDet.getString("EmplID"), themeDisplay));
				     employeesAbsenceCancelDetailList.put(i,tempEmployeeAppRefDet);
			     }
		      LOGGER.info("approverefuse-absence  Format API response : getApproveRefuseAbsListDetailsApiJsonResponse: "+employeesAbsenceCancelDetailList);
			  return  employeesAbsenceCancelDetailList;
		   }
		    catch(JSONException err){
		    	LOGGER.error("approverefuse-absence JSON Error "+err.toString() );
		    	return null;
		    }
		
	}   
}
