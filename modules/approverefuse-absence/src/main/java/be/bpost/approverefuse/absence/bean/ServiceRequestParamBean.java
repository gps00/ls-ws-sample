package be.bpost.approverefuse.absence.bean;

import java.io.Serializable;

public class ServiceRequestParamBean implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4711712488523136425L;
	
	public ServiceRequestParamBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	private String reqId;
	private String env;
	private String apiPath;
	private String bodyEntity;
	private String empId;
	private String requestType;
	private String requestedBy;
	private String requestDetail;
	
	public String getReqId() {
		return reqId;
	}
	public void setReqId(String reqId) {
		this.reqId = reqId;
	}
	public String getEnv() {
		return env;
	}
	public void setEnv(String env) {
		this.env = env;
	}
	public String getApiPath() {
		return apiPath;
	}
	public void setApiPath(String apiPath) {
		this.apiPath = apiPath;
	}
	public String getBodyEntity() {
		return bodyEntity;
	}
	public void setBodyEntity(String bodyEntity) {
		this.bodyEntity = bodyEntity;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public String getRequestedBy() {
		return requestedBy;
	}
	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}
	public String getRequestDetail() {
		return requestDetail;
	}
	public void setRequestDetail(String requestDetail) {
		this.requestDetail = requestDetail;
	}
	
	

}
