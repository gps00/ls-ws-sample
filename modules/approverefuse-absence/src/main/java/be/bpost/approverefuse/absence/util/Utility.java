package be.bpost.approverefuse.absence.util;

import java.security.Key;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;

public class Utility {
	private static final Log LOGGER = LogFactoryUtil.getLog(Utility.class);
	private String userImageUrl=null;
	private static final String SECRET_KEY="p1kzPHAkT7Xlqc01";
	
	public String retrieveUserImageUrl(String reqId,long companyID, String username, ThemeDisplay themeDisplay) {
		
		try {
			User user = UserLocalServiceUtil.getUserByScreenName(companyID, username);
			userImageUrl=user.getPortraitURL(themeDisplay);
			return userImageUrl;
		} catch (PortalException e) {
			LOGGER.info("approverefuse-absence Utility class "+reqId+":Error retreiving image for "+ companyID+ " : "+ username+ " error "+ e.getMessage() );
			return null;
		}
		
	}
	 public static String getDecryptText(String encodedText)throws Exception {
	     Cipher aesCipher = Cipher.getInstance("AES");
	     Key aeskey = new SecretKeySpec(SECRET_KEY.getBytes(),"AES");
	     aesCipher.init(Cipher.DECRYPT_MODE, aeskey);
	     byte[] bytePlainText = aesCipher.doFinal(Base64.getDecoder().decode(encodedText));
	     return new String(bytePlainText);
	 }
}