package be.bpost.approverefuse.absence.constants;

/**
 * @author u514720
 */
public class ApproverefuseAbsencePortletKeys {

	public static final String ApproverefuseAbsence = "approverefuseabsence";

}