package be.bpost.approverefuse.absence.util;

import java.util.HashMap;
import java.util.Map;

public class ApiGatewayMapping {
	
	public static  ApiHeaders getApiHeaders(String env){
		Map<String, ApiHeaders> apiHeaderMap = new HashMap<String, ApiHeaders >();		
		apiHeaderMap.put("LOCAL", new ApiHeaders("jLRAeRGFoY1pvycEZU2XS18dUJgmqMwb6985sQ6t","https://api-bpost4me-web-st1.bpost.cloud/","bpost4me-web","m9#RsB*VH@Sjs445kDTDu4z4a?X9H&B+zRL&C2dY_FqmM!WZZA5#r?+eF$hb6PS%&Sw67C9Jg*65E-^?vQ4HELC4Wn875=qfx@yJ7MUPF&J+@F?sBuVPYU-fd^$quAE^tP*%dPfwjkY7#XA=8YBEE=vgfze8jRdCxu#!B_&+eD8WRaAwKGPtcu=M#f6YpZ@NXS7jGVnTGVFXCw=?n5qFd5x=CMjTKwjtDcJWpv2G+4mH4DjpXqStM*2SP2dx!jS@"));
		apiHeaderMap.put("DEV", new ApiHeaders("jLRAeRGFoY1pvycEZU2XS18dUJgmqMwb6985sQ6t","https://api-bpost4me-web-st1.bpost.cloud/","bpost4me-web","m9#RsB*VH@Sjs445kDTDu4z4a?X9H&B+zRL&C2dY_FqmM!WZZA5#r?+eF$hb6PS%&Sw67C9Jg*65E-^?vQ4HELC4Wn875=qfx@yJ7MUPF&J+@F?sBuVPYU-fd^$quAE^tP*%dPfwjkY7#XA=8YBEE=vgfze8jRdCxu#!B_&+eD8WRaAwKGPtcu=M#f6YpZ@NXS7jGVnTGVFXCw=?n5qFd5x=CMjTKwjtDcJWpv2G+4mH4DjpXqStM*2SP2dx!jS@"));
		apiHeaderMap.put("UAT", new ApiHeaders("sYk9hWV3al4PtNYMxg7kN3TbwmQci67p1vubCk5H","https://api-bpost4me-web-ac1.bpost.cloud/","bpost4me-web","pCg$S9pum-gDxdJP*YEV!7Yt7Nfk6JK7HFaqUr!tprE-+LSV$pNGVX85K7UD+SP7jNVjp4Phw7x4eFBtaGM!Tv7^mK*8umWTrDKbcHV#Yf@cA_V%Y9*^^6rj^?6q8_gfts--qtn^AcNJQyHr6Kw*#z?F5cE3gB+nFtW&!ESCR$aq45ZUK$T2+7s@&fz%PUBm2D8v#_-*4jS=jf9GN=r8+54xC25Kf-H#F4z3qXV2Fyf@uE%jeCj+vK_c=mbTXLh*"));
		apiHeaderMap.put("PROD",new ApiHeaders("0Kp9McP9PT7Xlp1kzPqc01IQduHAkAKb74iPK6Oz","https://api-bpost4me-web.bpost.cloud/","bpost4me-web","RKkbsf^RYb$dc=npsU*!P3!$HL@MDmnS9?*tbUc5JMxh$@Bvx%hk4Fm=GGH7X!#7a%YcR6!%G_Ps^#=9aVLq_Wwxdyw+QAT7HsEvU_yRBDS%&N-YB%Szzm3L2mJwkv?@%u=DsbJX=7ZmrADxnN2XJxBz@ZGUh*yA3Ky38ja=J%G#hWt8kDKJqK^AYbyxUm%m&8Ej9j7Nbmz?8UqScg#KT7BbyAQ^tdWMtFSnx*X!Ty^KjG7C^m+G3_ptU9qh6-%_"));
		
		return apiHeaderMap.get(env);
	}

}