package be.bpost.approverefuse.absence.util;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;

public class CreateJwtToken {
   
	private static final Log LOGGER = LogFactoryUtil.getLog(CreateJwtToken.class);
   
	String createJwtToken(String reqId,String user, String key, String env){
		
		LOGGER.info("ApproverefuseAbsencePortlet JWT token "+ reqId+": creating token for :"+ user +" :"+ env);
		
		try {
			Calendar c = Calendar.getInstance();
			Date now = c.getTime();
			c.add(Calendar.MINUTE, 15);
			Date expirationDate = c.getTime();
			Map<String, Object> headerClaims = new HashMap<String, Object>();
		    Algorithm algorithm = Algorithm.HMAC256(key);
		    String token = JWT.create().withExpiresAt(expirationDate)
		    	.withIssuedAt(now)
		    	.withNotBefore(now)
		    	.withHeader(headerClaims)
		    	.withSubject(user)
		        .withIssuer(env)
		        .sign(algorithm);
		   LOGGER.info("ApproverefuseAbsencePortlet JWT token: token created "+ reqId+":"+ token);
		   return token;
		} catch (JWTCreationException exception){
		  LOGGER.error("ApproverefuseAbsencePortlet  JWT token "+ reqId+": error while creating token");
		  LOGGER.error(exception.getMessage());
		  return null;
		  //Invalid Signing configuration / Couldn't convert Claims.
		}
		
	}
}
