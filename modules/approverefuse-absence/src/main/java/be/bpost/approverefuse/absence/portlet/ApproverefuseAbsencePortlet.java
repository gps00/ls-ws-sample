package be.bpost.approverefuse.absence.portlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.UUID;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.commons.text.StringEscapeUtils;
import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;

import be.bpost.approverefuse.absence.bean.ServiceRequestParamBean;
import be.bpost.approverefuse.absence.constants.ApproverefuseAbsencePortletKeys;
import be.bpost.approverefuse.absence.util.FormattedApiResponse;
import be.bpost.approverefuse.absence.util.HttpRestServiceClient;
import be.bpost.approverefuse.absence.util.Utility;

/**
 * @author u514720
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=bpost.home",
        "com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.instanceable=true", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + ApproverefuseAbsencePortletKeys.ApproverefuseAbsence,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class ApproverefuseAbsencePortlet extends MVCPortlet {

	private static final Log LOGGER = LogFactoryUtil.getLog(ApproverefuseAbsencePortlet.class);
	private static String GET_APR_REF_ABS_LIST_API = "getApproveRefuseAbsList";
	private static String POST_APR_REF_ABS_API = "postApproveRefuseAbs";
	private static String MSS_REQUEST_TYPE = "MSS";

	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		String encId = null;
		LOGGER.info("approve-refuse Portlet: doView: inside");
		try {
			if (null != PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest))
					.getParameter("appuser")) {
				encId = Utility.getDecryptText(
						PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest))
								.getParameter("appuser"));
				LOGGER.info("approve-refuse Portlet: doView: encId:" + encId);
				renderRequest.setAttribute("encId", encId);
			} else {
				renderRequest.setAttribute("encId", null);
			}
		} catch (Exception e1) {
			LOGGER.error("approve-refuse Portlet Exception occured in : doView method: " + e1.getMessage());
			e1.printStackTrace();
		}
		super.doView(renderRequest, renderResponse);
	}

	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws IOException, PortletException {
		LOGGER.info("ApproverefuseAbsencePortlet: serveResource");
		String requestType = MSS_REQUEST_TYPE;
		String resourceId = resourceRequest.getResourceID();
		FormattedApiResponse formatApiResponse = new FormattedApiResponse();
		ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String enviorment = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(resourceRequest))
				.getServerName();
		long companyID = PortalUtil.getCompanyId(resourceRequest);
		String env = formatApiResponse.getEnv(enviorment);
		String empId = themeDisplay.getUser().getScreenName();
		UUID uuid = UUID.randomUUID();
		String reqId = uuid.toString();
		ServiceRequestParamBean serviceRequestParamBean = new ServiceRequestParamBean();
		serviceRequestParamBean.setEnv(env);
		serviceRequestParamBean.setReqId(reqId);
		serviceRequestParamBean.setRequestedBy(themeDisplay.getUser().getScreenName());
		if (resourceId.matches("get")) {
			try {
				HttpRestServiceClient httpRestServiceClient = new HttpRestServiceClient();
				// Logic Added For MSS handling MSS USers- START
				BufferedReader reader = resourceRequest.getReader();
				String line = reader.readLine();
				com.liferay.portal.kernel.json.JSONObject data = JSONFactoryUtil.createJSONObject(line);
				String encId = null;
				LOGGER.info("data---------" + data);
				if (!data.isNull("encId")) {
					encId = (String) data.get("encId");
					LOGGER.info("approve-refuse encId in serveResource: " + encId);
					if (!encId.contains("null")) {
						empId = encId;
						requestType = MSS_REQUEST_TYPE;
						LOGGER.info("approve-refuse encId inside null check :" + empId);
					}
				}
				// Logic Added For MSS handling MSS USers- END
				String bodyEntity = "{\"employeeId\":\"" + empId + "\"}";
				serviceRequestParamBean.setEmpId(empId);
				serviceRequestParamBean.setRequestType(requestType);
				serviceRequestParamBean.setApiPath(GET_APR_REF_ABS_LIST_API);
				serviceRequestParamBean.setBodyEntity(bodyEntity);
				JSONObject approveRefuseAbsListResponse = httpRestServiceClient
						.getServiceResponse(serviceRequestParamBean);
				LOGGER.info("ApproverefuseAbsencePortlet : serveResource:: getApproveRefuseAbsListAPIResponse: "
						+ approveRefuseAbsListResponse);
				if (approveRefuseAbsListResponse.get("statusCode").toString().equalsIgnoreCase("200")) {
					String escapedString = StringEscapeUtils
							.unescapeJava(approveRefuseAbsListResponse.getString("response")).replace("\"{", "{");
					org.json.JSONArray jsonArray = null;
					org.json.JSONObject jsonObj = new org.json.JSONObject(escapedString.trim());
					if (jsonObj.getJSONObject("BPO_REST_ABS_PEND_RES1").length() > 0) {
						jsonArray = jsonObj.getJSONObject("BPO_REST_ABS_PEND_RES1")
								.getJSONArray("BPO_REST_ABS_PEND_RES");
						if (null != jsonArray)
							jsonArray = formatApiResponse.getApproveRefuseAbsListDetailsApiJsonResponse(jsonArray,
									reqId, companyID, themeDisplay);
					}
					resourceResponse.setProperty(ResourceResponse.HTTP_STATUS_CODE,
							approveRefuseAbsListResponse.get("statusCode").toString());
					resourceResponse.getWriter().println(jsonArray);
				} else if (approveRefuseAbsListResponse.get("statusCode").toString().equalsIgnoreCase("202")) {
					resourceResponse.setProperty(ResourceResponse.HTTP_STATUS_CODE,
							approveRefuseAbsListResponse.get("statusCode").toString());
					resourceResponse.getWriter().println();
				} else {
					resourceResponse.setProperty(ResourceResponse.HTTP_STATUS_CODE,
							approveRefuseAbsListResponse.get("statusCode").toString());
					resourceResponse.getWriter().println(approveRefuseAbsListResponse.getString("response"));
				}
			} catch (Exception e) {
				LOGGER.error("ApproverefuseAbsencePortlet:serveResource: get: Exception: " + e.getMessage());
				e.printStackTrace();
			}
			PrintWriter out = resourceResponse.getWriter();
			out.flush();
			out.close();
		} else if (resourceId.matches("send")) {
			try {
				HttpRestServiceClient httpRestServiceClienForPost = new HttpRestServiceClient();
				BufferedReader reader = resourceRequest.getReader();
				String line = reader.readLine();
				org.json.JSONObject data = new org.json.JSONObject(line);
				String emp_id = (String) data.get("empId");
				//String emp_id = "000000";
				String pin_name = (String) data.get("pin_name");
				// String pin_name = "ANN CT";
				String start_date = (String) data.get("start_date");
				String end_date = (String) data.get("end_date");
				//String start_date = "2021-01-11";
				//String end_date = "2021-01-18";
				String status_flag = (String) data.get("status_flag");
				//String status_flag = "DNY";
				String comments = (String) data.get("comments");
				//String comments = "test";
				String bodyEntity = "{\"employeeId\":\"" + emp_id + "\",\"pinName\":\"" + pin_name
						+ "\",\"startDate\":\"" + start_date + "\",\"endDate\":\"" + end_date + "\",\"statusFlag\":\""
						+ status_flag + "\",\"comments\":\"" + comments + "\",\"approverId\":\"" + empId + "\"}";
				serviceRequestParamBean.setEmpId(emp_id);
				serviceRequestParamBean.setApiPath(POST_APR_REF_ABS_API);
				serviceRequestParamBean.setBodyEntity(bodyEntity);
				serviceRequestParamBean.setRequestType(requestType);
				JSONObject postApproveRefuseAbsAPIResponse = httpRestServiceClienForPost
						.getServiceResponse(serviceRequestParamBean);
				LOGGER.info("ApproverefuseAbsencePortlet: serveResource:: postApproveRefuseAbsAPIResponse "
						+ postApproveRefuseAbsAPIResponse);
				if (postApproveRefuseAbsAPIResponse.get("statusCode").toString().equalsIgnoreCase("200")) {
					String escapedString = StringEscapeUtils
							.unescapeJava(postApproveRefuseAbsAPIResponse.getString("response")).replace("\"{", "{");
					org.json.JSONObject jsonObj = new org.json.JSONObject(escapedString.trim());
					org.json.JSONObject bposrestabspendingresponse = jsonObj.getJSONObject("BPO_REST_ABS_PENDING_RES");
					resourceResponse.setProperty(ResourceResponse.HTTP_STATUS_CODE,
							postApproveRefuseAbsAPIResponse.get("statusCode").toString());
					resourceResponse.getWriter().println(bposrestabspendingresponse);
				} else if (postApproveRefuseAbsAPIResponse.get("statusCode").toString().equalsIgnoreCase("202")) {
					resourceResponse.setProperty(ResourceResponse.HTTP_STATUS_CODE,
							postApproveRefuseAbsAPIResponse.get("statusCode").toString());
					resourceResponse.getWriter().println();
				} else {
					resourceResponse.setProperty(ResourceResponse.HTTP_STATUS_CODE,
							postApproveRefuseAbsAPIResponse.get("statusCode").toString());
					resourceResponse.getWriter().println(postApproveRefuseAbsAPIResponse.getString("response"));
				}
			} catch (Exception e) {
				LOGGER.error("ApproverefuseAbsencePortlet:serveResource: send: Exception: " + e.getMessage());
				e.printStackTrace();
			}
			PrintWriter out = resourceResponse.getWriter();
			out.flush();
			out.close();
		}
	}
}