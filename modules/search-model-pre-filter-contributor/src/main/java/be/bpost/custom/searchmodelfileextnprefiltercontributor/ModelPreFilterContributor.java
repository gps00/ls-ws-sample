/**
 * Copyright 2000-present Liferay, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.bpost.custom.searchmodelfileextnprefiltercontributor;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroup;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.filter.BooleanFilter;
import com.liferay.portal.kernel.search.filter.TermsFilter;
import com.liferay.portal.kernel.service.RoleLocalServiceUtil;
import com.liferay.portal.kernel.service.UserGroupLocalServiceUtil;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;


import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * @author u514720
 */
@Component(
	immediate = true,service = com.liferay.portal.search.spi.model.query.contributor.QueryPreFilterContributor.class
)


public class ModelPreFilterContributor
	implements com.liferay.portal.search.spi.model.query.contributor.QueryPreFilterContributor {
	private static final Log LOGGER = LogFactoryUtil.getLog(ModelPreFilterContributor.class);
	@Override
	public void contribute(BooleanFilter fullQueryBooleanFilter, SearchContext searchContext) {
		try {
			User user = UserLocalServiceUtil.getUser(searchContext.getUserId());
			UserGroup ugg = UserGroupLocalServiceUtil.fetchUserGroup(user.getCompanyId(),
					"GU-ADM-HOM-LifeRayAccess-ADMIN");
			Role roleComCal = RoleLocalServiceUtil.getRole(user.getCompanyId(), "MSO-Comcaldr-Admin-Role");
			Role roleAdmin = RoleLocalServiceUtil.getRole(user.getCompanyId(), "Administrator");
			if (!(UserGroupLocalServiceUtil.hasUserUserGroup(searchContext.getUserId(), ugg.getUserGroupId())
					|| RoleLocalServiceUtil.hasUserRole(searchContext.getUserId(), roleComCal.getRoleId())
					|| RoleLocalServiceUtil.hasUserRole(searchContext.getUserId(), roleAdmin.getRoleId()))) {
				TermsFilter filter = new TermsFilter("extension");
				filter.addValues("jpeg", "jpg", "png", "tif", "gif", "bmp", "psd");
				fullQueryBooleanFilter.add(filter, BooleanClauseOccur.MUST_NOT);
			}

		} catch (Exception e) {
			LOGGER.error("ModelPreFilterContributor: contribute:Error: "+e.getMessage());
		}

	}

}