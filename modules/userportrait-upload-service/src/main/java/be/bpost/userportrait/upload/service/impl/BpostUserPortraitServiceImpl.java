/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package be.bpost.userportrait.upload.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.activation.MimetypesFileTypeMap;
import com.liferay.portal.kernel.exception.NoSuchUserException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;

import be.bpost.userportrait.upload.service.base.BpostUserPortraitServiceBaseImpl;

/**
 * The implementation of the bpost user portrait remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link be.bpost.userportrait.upload.service.BpostUserPortraitService}
 * interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have
 * security checks based on the propagated JAAS credentials because this service
 * can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BpostUserPortraitServiceBaseImpl
 * @see be.bpost.userportrait.upload.service.BpostUserPortraitServiceUtil
 */
public class BpostUserPortraitServiceImpl extends BpostUserPortraitServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link
	 * be.bpost.userportrait.upload.service.BpostUserPortraitServiceUtil} to access
	 * the bpost user portrait remote service.
	 */
	private static final Log LOGGER = LogFactoryUtil.getLog(BpostUserPortraitServiceImpl.class);
	public JSONObject updateUserPortrait(String screenName, File file) {
		LOGGER.info("updateUserPortrait STARTED for : " +screenName);
		JSONObject jsonobject = JSONFactoryUtil.createJSONObject();
		if (null != screenName && (!screenName.isEmpty()) && file.exists() && file.isFile()) {
			String mimetype = new MimetypesFileTypeMap().getContentType(file);
			String type = mimetype.split("/")[0];
			if (type.equals("image")) {
				double imgSizeinkb = (file.length() / 1024);
				if (imgSizeinkb <= 2150.4) {
					Long companyId = CompanyThreadLocal.getCompanyId();
					FileInputStream fileInputStream = null;
					byte[] bytesArray = null;
					try {
						Long userId = UserLocalServiceUtil.getUserIdByScreenName(companyId, screenName);
						bytesArray = new byte[(int) file.length()];
						fileInputStream = new FileInputStream(file);
						fileInputStream.read(bytesArray); // read file into bytes[]
						UserLocalServiceUtil.updatePortrait(userId, bytesArray);
						jsonobject.put("Status", "SUCCESS");
						jsonobject.put("StatusMessage", "Portrait image successfully uploaded for : " + screenName);
						LOGGER.info("Status: SUCCESS StatusMessage: Portrait image successfully uploaded for : "+ screenName);

					} catch (NoSuchUserException nse) {
						nse.printStackTrace();
						jsonobject.put("Status", "FAILED");
						jsonobject.put("StatusMessage",
								" NoSuchUserException: occured for : " +screenName+ " is not present in Bpost4me.");
						LOGGER.error("Status: FAILED StatusMessage:  NoSuchUserException: occured for :  "+screenName+" is not present in Bpost4me: "+nse.getMessage());
						return jsonobject;
					} catch (IOException ie) {
						ie.printStackTrace();
						jsonobject.put("Status", "FAILED");
						jsonobject.put("StatusMessage", "IOException occured for : " + screenName);
						LOGGER.error("Status: FAILED StatusMessage:  IOException:  occured for  "+screenName+ " : "+ie.getMessage());
						return jsonobject;
					}

					catch (Exception e) {
						e.printStackTrace();
						jsonobject.put("Status", "FAILED");
						jsonobject.put("StatusMessage",
								"Some exception occured for : " +screenName);
						LOGGER.error("Status: FAILED StatusMessage:  Some exception occured for :  "+screenName+" : "+e.getMessage());
						return jsonobject;
					} finally {
						if (fileInputStream != null) {
							try {
								fileInputStream.close();
							} catch (IOException e) {
								e.printStackTrace();
								LOGGER.error("IOException  occured in finally block for :  "+screenName+" : "+e.getMessage());
							}
						}

					}
				} else {
					jsonobject.put("Status", "FAILED");
					jsonobject.put("StatusMessage", "Image size > 2MB for:" +screenName);
					LOGGER.error("Status: FAILED StatusMessage:  Image size > 2MB for:  "+screenName);
					return jsonobject;
				}
			} else {
				jsonobject.put("Status", "FAILED");
				jsonobject.put("StatusMessage", "File must be an image for:" + screenName);
				LOGGER.error("Status: FAILED StatusMessage:  File must be an image for:  "+screenName);
				return jsonobject;
			}
		} else {
			jsonobject.put("Status", "FAILED");
			jsonobject.put("StatusMessage", "ScreenName or File cannot be empty for:" +screenName);
			LOGGER.error("Status: FAILED StatusMessage:  ScreenName or File cannot be empty for : "+screenName);
			return jsonobject;
		}

		return jsonobject;

	}

}