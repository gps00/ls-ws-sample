package be.bpost.mrs.ppref.util;

import java.util.List;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.liferay.mail.kernel.service.MailServiceUtil;
import com.liferay.mail.kernel.model.MailMessage;

import be.bpost.mrs.ppref.servicebuilder.model.MrsselectiontoolUseroffices;


public class EmailServiceUtil {
	//private static final Log LOGGER = LogFactory.getLog(EmailServiceUtil.class);
	private static final Log LOGGER = LogFactory.getLog(EmailServiceUtil.class);

	
	public static boolean sendEmail(List<MrsselectiontoolUseroffices> officeList,
			String toEmail,String fromEmail, String lang) {
		boolean flag = false;
		
		try {
			InternetAddress from = new InternetAddress();
			from.setAddress(fromEmail);
			// from.setPersonal(propsProvider.getAdminEmailFromName(), "utf-8");
			String subject;
			if(lang.equalsIgnoreCase("fr_FR"))
				subject = "Zone de Planification Individuelle - Ma liste de choix";
			else
				subject = "Individuele Planningzone - Mijn keuzelijst";
			String body = createMailBody(officeList,lang);
			MailMessage mail = new MailMessage();
			mail.setFrom(from);
			mail.setHTMLFormat(true);
			mail.setBody(body);
			mail.setSubject(subject);
			mail.setTo(new InternetAddress(toEmail));
			MailServiceUtil.sendEmail(mail);
		} catch (AddressException e) {
			 LOGGER.error("FAILED-Mail to -"+ toEmail);
			//System.out.println(e.getMessage());
		};
         
           LOGGER.info("Mail sent to -"+ toEmail);
		return flag;
	}
	

	private static String createMailBody(List<MrsselectiontoolUseroffices> officeList, String lang) {
		StringBuilder stringBuilder = new StringBuilder();

		if (lang.equalsIgnoreCase("fr_FR")) {
			stringBuilder.append("<p>Cher (Chère) collègue,").append("</p>");
			stringBuilder
					.append("Veuillez trouver ci-dessous votre liste de choix dans le contexte des « Zones de Planification Individuelles ».")
					.append("</br></br>").append("<table style='border:1px solid black'>");
		} else {
			stringBuilder.append("<p>Beste Collega, ").append("</p>");
			stringBuilder.append("Hierna vind je je keuzelijst in het kader van “Individuele Planningzones”.")
					.append("</br></br>").append("<table style='border:1px solid black'>");
		}

		for (MrsselectiontoolUseroffices mrsoffc : officeList) {
			if (!mrsoffc.isIsDeleted())
				if (lang.equalsIgnoreCase("fr_FR")) {
					stringBuilder.append("<tr><td>");
					stringBuilder.append(mrsoffc.getOfficeNameFr()).append("</td></tr>");
				} else {
					stringBuilder.append("<tr><td>");
					stringBuilder.append(mrsoffc.getOfficeNameNL()).append("</td></tr>");
				}
		}
		LOGGER.info("Mail body -" + stringBuilder.toString());
		return stringBuilder.toString();
	}

}
