/**
 * Copyright 2000-present Liferay, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.bpost.mrs.ppref.controller;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import be.bpost.mrs.ppref.servicebuilder.model.MrsselectiontoolUseroffices;
import be.bpost.mrs.ppref.servicebuilder.model.MrsselectiontoolUsers;
import be.bpost.mrs.ppref.servicebuilder.service.MrsselectiontoolUserofficesLocalService;
import be.bpost.mrs.ppref.servicebuilder.service.MrsselectiontoolUserofficesLocalServiceUtil;
import be.bpost.mrs.ppref.servicebuilder.service.MrsselectiontoolUsersLocalService;
import be.bpost.mrs.ppref.util.EmailServiceUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author Liferay
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=bpost.home",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=planning-preference",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/jsp/view.jsp",
		"javax.portlet.name=be_bpost_planning-preference_web",
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user,guest,administrator"
	},
	service = Portlet.class
)
public class PlanningPreferenceController extends MVCPortlet {
	
	private static final Log LOGGER = LogFactory.getLog(PlanningPreferenceController.class);


	public MrsselectiontoolUsersLocalService getMrsselectiontoolUsersLocalService() {
		return _mrsselectiontoolUsersLocalService;
	}
	public MrsselectiontoolUserofficesLocalService getMrsselectiontoolUserofficesLocalService() {
		return _mrsselectiontoolUserofficesLocalService;
	}

	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {

		super.doView(renderRequest, renderResponse);

	}

	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws IOException, PortletException {
		String resourceId = resourceRequest.getResourceID();
		ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String lang = ParamUtil.get(resourceRequest, "lang", StringPool.BLANK);
		JSONObject jo = JSONFactoryUtil.createJSONObject();
		int empId = Integer.parseInt(themeDisplay.getUser().getScreenName());
		
		String name = themeDisplay.getUser().getFullName();
		String email = themeDisplay.getUser().getEmailAddress();
		String empOffices = null;
		String homeAddress = null;
		String pUnit = null;
		boolean isAllowed = false;
		String message = "";
		if (resourceId.matches("get")) {
			try {
				homeAddress =getMrsselectiontoolUsersLocalService().getMrsselectiontoolUsers(empId).getHomeAddress();
				isAllowed = getMrsselectiontoolUsersLocalService().getMrsselectiontoolUsers(empId).isIsAllowed();
				pUnit = getMrsselectiontoolUsersLocalService().getMrsselectiontoolUsers(empId).getPlanningUnit();
				List<MrsselectiontoolUseroffices> officeList = getMrsselectiontoolUserofficesLocalService()
						.getOfficebyEmployeeId(empId);
				empOffices = JSONFactoryUtil.looseSerialize(officeList);
				
                if (officeList.isEmpty() && isAllowed)
					message = "nooffice";
			} catch (Exception e) {
				if (e.getMessage().contains("No MrsselectiontoolUsers exists with the primary key"))
					message = "NoMrsselectiontoolUsersWithPrimaryKey";
				else {
					if (lang.equalsIgnoreCase("fr_FR")) {

						message = "L’application est temporairement indisponible, veuillez réessayer ultérieurement.";
					} else {
						message = "De applicatie is momenteel niet beschikbaar, probeer later opnieuw.";
					}
				}
				LOGGER.error("Exception from get:empid-" + empId + ":isAllowed:" + isAllowed + ":" + e);
				e.printStackTrace();
			}
			jo.put("offices", empOffices);
			jo.put("name", name);
			jo.put("id", empId);
			jo.put("homeAddress", homeAddress);
			jo.put("beginMessage", message);
			jo.put("isAllowed", isAllowed);
			jo.put("pUnit", pUnit);
			PrintWriter out = resourceResponse.getWriter();
			out.println(jo);
			out.flush();
			out.close();
		} else if (resourceId.matches("send")) {
			System.out.println("Inside Send");
			String param = ParamUtil.get(resourceRequest, "data", StringPool.BLANK);
			param = param.replaceAll("\\\\n", "");

			JsonParser parser = new JsonParser();
			JsonArray json = (JsonArray) parser.parse(param);
			List<MrsselectiontoolUseroffices> officeList = new ArrayList<MrsselectiontoolUseroffices>();
			try {
				for (int i = 0; i < json.size(); i++) {
					JsonObject temp = (JsonObject) json.get(i);
					MrsselectiontoolUseroffices offices;

					offices = MrsselectiontoolUserofficesLocalServiceUtil
							.getMrsselectiontoolUseroffices(Integer.parseInt(temp.get("RId").toString()));
					offices.setIsDeleted(temp.get("isDeleted").getAsBoolean());
					offices.setOfficePriority(Integer.parseInt(temp.get("officePriority").toString()));

					MrsselectiontoolUserofficesLocalServiceUtil.updateMrsselectiontoolUseroffices(offices);
					officeList.add(offices);
				}
				MrsselectiontoolUsers user = getMrsselectiontoolUsersLocalService().getMrsselectiontoolUsers(empId);
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				user.setPreferencesSet(timestamp);
				getMrsselectiontoolUsersLocalService().updateMrsselectiontoolUsers(user);
				EmailServiceUtil.sendEmail(officeList, email,
						"no-reply@bpost.be", lang);
				if (lang.equalsIgnoreCase("fr_FR")) {
					jo.put("message", "Votre demande a bien été envoyée.");
				} else {
					jo.put("message", "Uw aanvraag is succesvol ingediend.");
				}
			} catch (Exception e) {
				if (lang.equalsIgnoreCase("fr_FR")) {
					jo.put("message",
							"L’application est temporairement indisponible, veuillez réessayer ultérieurement.");
				} else {
					jo.put("message", "De applicatie is momenteel niet beschikbaar, probeer later opnieuw.");
				}
				LOGGER.error("Exception from send:empid-" + empId + ":" + e);
				e.printStackTrace();

			}
			PrintWriter out = resourceResponse.getWriter();
			out.println(jo);
			out.flush();
			out.close();
		}
		super.serveResource(resourceRequest, resourceResponse);
	}


	
	@Reference
	private volatile MrsselectiontoolUsersLocalService _mrsselectiontoolUsersLocalService;
	@Reference
	private volatile MrsselectiontoolUserofficesLocalService _mrsselectiontoolUserofficesLocalService;

}

