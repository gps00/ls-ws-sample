<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
<portlet:defineObjects />
<liferay-portlet:resourceURL var="getData" id="get">
</liferay-portlet:resourceURL>

<liferay-portlet:resourceURL var="sendData" id="send">
</liferay-portlet:resourceURL>

<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/select.css">
<script src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui.min.js"></script>
<script src="<%=request.getContextPath()%>/js/angular.min.js"></script>
<script src="<%=request.getContextPath()%>/js/angular-touch.min.js"></script>
<script src="<%=request.getContextPath()%>/js/angular-animate.min.js"></script>
<script src="<%=request.getContextPath()%>/js/angular-sanitize.js"></script>
<script
	src="<%=request.getContextPath()%>/js/ui-bootstrap-tpls-2.5.0.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery.ui.touch-punch.js"></script>
<script src="<%=request.getContextPath()%>/js/sortable.js"></script>
<script src="<%=request.getContextPath()%>/js/select.js"></script>
<style>
.navbar {
	position: inherit !important;
}

.modal-body {
	word-break: break-all;
	text-align: center;
}

.modal-open .modal {
	overflow-y: hidden;
    top: 10%;
    margin: auto;
}

#mobile-heading {
	z-index: 1030;
}

.selected-office {
	height: 663px;
	overflow-y: auto;
	overflow-x: hidden;
}

.home-office {
	height: 145px;
	overflow-y: auto;
	overflow-x: hidden;
	background-color: #D6EAF8;
}
.pu-office {
	height: 145px;
	overflow-y: auto;
	overflow-x: hidden;
	background-color: #F9E79F;
}
.btn-warning {  
    padding: 8px 12px;
    text-transform: uppercase;
    min-width: 120px;
    background-image: -moz-linear-gradient(top,#ffa658,#f49645);
    background-image: -webkit-gradient(linear,0 0,0 100%,from(#ffa658),to(#f49645));
    background-image: -webkit-linear-gradient(top,#ffa658,#f49645);
    background-image: -o-linear-gradient(top,#ffa658,#f49645);
    background-image: linear-gradient(to bottom,#ffa658,#f49645);
    border-color: #f49645 #f49645 #df6e0d;
    border-color: rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);
    font-weight: normal;
    font-size: 15px;
}
.btn-primary {
    min-width: 120px;
    padding: 8px 12px;
    font-weight: normal;
    font-size: 15px;
}
@media ( max-width : 1120px) {
	.row-fluid {
		width: 98%;
	}
	.row-fluid .span12 {
		width: 102%;
	}
}

.navbar {
	position: inherit;
}

#heading h1.site-title h1.small {
	font-size: 100%;
	font-weight: bold;
	line-height: 40.5px;
}

.topnav .row {
	margin: 0;
}

@media ( max-width : 979px) {
	.aui #wrapper {
		z-index: unset;
	}
}

.modal-backdrop, .modal-backdrop.fade.in {
	opacity: 0.5;
	filter: alpha(opacity = 50);
}

.sub-button {
	margin-left: 0;
}

@media ( max-width : 767px) {
	.row {
		margin-left: 0;
		margin-right: 0;
	}
	.sub-button {
		margin-left: 0px;
	}
	.panel-default>.panel-body {
		font-size: 13px;
	}
	input.ui-select-search {
		font-size: 14px;
	}
	.ui-select-bootstrap .ui-select-placeholder {
		font-size: 14px;
	}
}

@media ( min-width : 768px) {
	.modal {
		width: 600px;
        top: 10%;
        margin: auto;
	}
	.modal-dialog {
		margin: 1rem auto;
		max-width: 500px;
	}
}
@media ( max-width : 767px) and (min-width: 576px){
.modal-dialog {
    max-width: none;
    margin: 0.5rem;
}
}
.panel {
	border-radius: 0px;
	box-shadow: 2px 4px 4px rgba(0, 0, 0, .05);
}

.panel-default {
	border-color: #fff;
}

.panel-default>.panel-heading {
	color: #ef2637;
	border-color: #ddd;
	font-size: 20px;
	font-weight: 400;
	border-radius: 0px;
	background-color: #fff;
	padding: 10px 15px;
}

.panel-default>.panel-body {
	padding: 0 15px;
}

.ui-select-bootstrap .ui-select-placeholder {
	text-transform: initial;
}

.ui-select-bootstrap .ui-select-placeholder:hover {
	text-decoration: none
}

.ui-select-bootstrap .ui-select-match .btn {
	padding: 7px 12px;
}

.ui-select-bootstrap .ui-select-match-text {
	color: #000;
	font-weight: normal;
}

input.ui-select-search {
	width: 97% !important;
}

.fa {
	cursor: pointer;
}

.list {
	margin: 10px 0;
	padding: 10px 0;
}

.office {
	background-color: bisque;
	padding: 10px;
	margin-bottom: 5px;
	cursor: move;
	border-radius: 7px;
}

.office.ng-animate {
	transition: all 0.5s ease;
	top: 0;
}

.office.ng-move+div {
	/* cannot have transition on this element */
	/*transition: all 1s ease;*/
	position: relative;
	top: -26px;
}

.office.ng-move.ng-move-active+div {
	transition: all 0.5s ease;
	position: relative;
	top: 0;
}

.office.ng-enter {
	position: relative;
	opacity: 0.0;
	height: 0;
	left: 100px;
}

.office.ng-enter.ng-enter-active {
	transition: all 0.5s ease;
	opacity: 1.0;
	left: 0;
	height: 26px;
}

.office.ng-leave {
	position: relative;
	opacity: 1.0;
	left: 0;
	height: 26px;
	z-index: -100;
}

.office.ng-leave.ng-leave-active {
	transition: all 0.5s ease;
	opacity: 0.0;
	left: 100px;
	height: 0;
	top: -13px;
}

.buttons {
	float: right;
}

.background-yellow {
	background-color: #FFC000;
}

.background-blue {
	background-color: #00B0F0;
	color: #fff;
}

.background-green {
	background-color: #00B050;
	color: #fff;
}

.background-red {
	background-color: #C00000;
	color: #fff;
}

.loader {
	display: none;
	width: 100%;
	position: fixed;
	top: 0;
	bottom: 0;
	right: 0;
	left: 0;
	background-color: black;
	opacity: 0.7;
	z-index: 1050;
}

.loading {
	position: absolute;
	width: 100px;
	height: 100px;
	top: 50%;
	left: 50%;
	margin: -50px 0 0 -50px;
}
</style>
<script type="text/javascript">
	var app = angular.module('myApp', [ 'ngAnimate', 'ngTouch', 'ngSanitize', 'ui.select',
			'ui.bootstrap', 'ui.sortable' ]);
	app.filter('propsFilter', function() {
		return function(items, props) {
			var out = [];

			if (angular.isArray(items)) {
				var keys = Object.keys(props);

				items
						.forEach(function(item) {
							var itemMatches = false;

							for (var i = 0; i < keys.length; i++) {
								var prop = keys[i];
								var text = props[prop].toLowerCase();
								if (item[prop].toString().toLowerCase().indexOf(
										text) !== -1) {
									itemMatches = true;
									break;
								}
							}

							if (itemMatches) {
								out.push(item);
							}
						});
			} else {
				// Let the output be the input untouched
				out = items;
			}

			return out;
		};
	});
	app.controller(
					'DemoCtrl',
					[
							'$scope',
							'$http',
							'$uibModal',
							'$timeout',
							'$interval',
							function($scope, $http, $uibModal, $timeout,
									$interval) {

								var getUrl = '${getData}';
								var sendUrl = '${sendData}';
								var vm = this;
								function compare(a, b) {
									if (a.officePriority < b.officePriority)
										return -1;
									if (a.officePriority > b.officePriority)
										return 1;
									return 0;
								}
								$scope.result = $http.get(getUrl.toString())
										.then(function(response) {
											return response.data;
										});
								$scope.result
										.then(function(data) {

											console.log(data);
											vm.clear = function() {
												vm.lanOffice.selected = undefined;
											};
											vm.appendToBodyDemo = {
												remainingToggleTime : 0,
												present : true,
												startToggleTimer : function() {
													var scope = vm.appendToBodyDemo;
													var promise = $interval(function() {
														if (scope.remainingTime < 1000) {
															$interval.cancel(promise);
															scope.present = !scope.present;
															scope.remainingTime = 0;
														} else {
															scope.remainingTime -= 1000;
														}
													}, 1000);
													scope.remainingTime = 3000;
												}
											};
											vm.lanOffice = {};
											$scope.id = data.id;
											$scope.name = data.name;
											$scope.homeAddress = data.homeAddress;
											$scope.isAllowed = data.isAllowed;
											$scope.beginMessage = data.beginMessage;
											$scope.offices=data.beginMessage;
											$scope.pUnit = data.pUnit;
											$scope.lang = "<%=pageContext.getRequest().getLocale()%>";
											
											
											if($scope.beginMessage != ""){
												if($scope.beginMessage=="NoMrsselectiontoolUsersWithPrimaryKey"){
													if($scope.lang == "fr_FR"){
													    $scope.beginMessage = "Vous n’avez pas accès à cet outil actuellement.  Si vous devez avoir les accès nécessaires pour communiquer vos choix, vous pouvez le signaler via la boîte mail MRSRetail.NetworkOrganisation@bpost.be";
													}else{
														$scope.beginMessage = "U heeft momenteel geen toegang tot deze tool.  Dien je wel toegang te hebben om je keuzes mee te delen kan je dit melden via de mailbox MRSRetail.NetworkOrganisation@bpost.be";
													}
												}else if($scope.beginMessage == "nooffice"){
													$scope.isAllowed == false;
													if($scope.lang == "fr_FR"){
													    $scope.beginMessage = "Vous n’avez pas la possibilité de déterminer vos préférences actuellement. Si vous devez le faire, vous pouvez le signaler via la boîte mail MRSRetail.NetworkOrganisation@bpost.be.";
													}else{
														$scope.beginMessage = "U heeft momenteel niet de mogelijkheid om je voorkeuren te bepalen. Dien je dit wel te doen, kan je dit melden via de mailbox MRSRetail.NetworkOrganisation@bpost.be.";
													}
												}
												var modalInstance = $uibModal.open({
												      animation: true,
												      ariaLabelledBy: 'modal-title',
												      ariaDescribedBy: 'modal-body',
												      templateUrl: 'beginningMessage.html',
												      controller: 'ModalInstanceCtrlBegin',
												      controllerAs: '$beginctrl',
												      scope : $scope
												    });
												    modalInstance.result.then(function () {
												    	
												    }, function () {
												   
												    });
											}else if($scope.beginMessage == "" && $scope.isAllowed == false){
												if($scope.lang == "fr_FR"){
												    $scope.beginMessage = "Vous n’avez pas la possibilité de déterminer vos préférences actuellement. Si vous devez le faire, vous pouvez le signaler via la boîte mail MRSRetail.NetworkOrganisation@bpost.be.";
												}else{
													$scope.beginMessage = "U heeft momenteel niet de mogelijkheid om je voorkeuren te bepalen. Dien je dit wel te doen, kan je dit melden via de mailbox MRSRetail.NetworkOrganisation@bpost.be.";
												}
												var modalInstance = $uibModal.open({
												      animation: true,
												      ariaLabelledBy: 'modal-title',
												      ariaDescribedBy: 'modal-body',
												      templateUrl: 'beginningMessage.html',
												      controller: 'ModalInstanceCtrlBegin',
												      controllerAs: '$beginctrl',
												      scope : $scope
												    });
												    modalInstance.result.then(function () {
												    	
												    }, function () {
												   
												    });
											}
											$scope.offices = JSON
													.parse(data.offices);

											$scope.selectedOffices = $
													.grep(
															$scope.offices,
															function(element,
																	index) {
																return element.isDeleted == false;
															});
											$scope.selectedOffices = $scope.selectedOffices
													.sort(compare);
											$scope.deselectedHomeOffices = $
													.grep(
															$scope.offices,
															function(element,
																	index) {
																return element.isDeleted == true
																		&& element.officeType == "H30";
															});
											$scope.deselectedPUOffices = $
													.grep(
															$scope.offices,
															function(element,
																	index) {
																return element.isDeleted == true
																		&& element.officeType == "P10";
															});
											vm.deselectedLANOffices = $.grep($scope.offices, function (element, index) {
											    return element.isDeleted == true && element.officeType=="LAN";
											});
											
											$scope.sortableOptions = {
												placeholder : 'office',
												connectWith : '.list',
												update : function(e, ui) {
								                    
									                    var dragModel = ui.item.sortable.source.attr('ng-model');
									                    var dropModel = ui.item.sortable.droptarget.attr('ng-model');
													    var dragIndex = ui.item.sortable.index;
													    var dropIndex = ui.item.sortable.dropindex;	
													    
													    ui.item.data('dragModel',dragModel);
													    ui.item.data('dropModel',dropModel);
													    ui.item.data('dragIndex',dragIndex);
													    ui.item.data('dropIndex',dropIndex);
								                    
												},
												stop: function(e, ui){
													var dragModel = ui.item.data('dragModel');
													var dropModel = ui.item.data('dropModel');
													var dragIndex = ui.item.data('dragIndex');
													var dropIndex = ui.item.data('dropIndex');
												
													if(dragModel == "selectedOffices" && dropModel == "deselectedHomeOffices"){
														var deselected = $scope.deselectedHomeOffices[dropIndex];
														if(deselected.officeType == "H30"){
															$scope.deselectedHomeOffices.splice(dropIndex, 1);
															$scope.deselectedHomeOffices.splice(dropIndex, 0, {
																PUDescription : deselected.PUDescription,
																PUNumber : deselected.PUNumber,
																RId : deselected.RId,
																employeeId : deselected.employeeId,
																isDeleted : true,
																officeDistance : deselected.officeDistance,
																officeLanguage : deselected.officeLanguage,
																officeNameFr : deselected.officeNameFr,
																officeNameNL : deselected.officeNameNL,
																officePersyId : deselected.officePersyId,
																officePriority : 0,
																officeRcCode : deselected.officeRcCode,
																officeType : deselected.officeType});
														}else {
															$scope.deselectedHomeOffices.splice(dropIndex, 1);
															$scope.selectedOffices.splice(dragIndex, 0, {
																PUDescription : deselected.PUDescription,
																PUNumber : deselected.PUNumber,
																RId : deselected.RId,
																employeeId : deselected.employeeId,
																isDeleted : deselected.isDeleted,
																officeDistance : deselected.officeDistance,
																officeLanguage : deselected.officeLanguage,
																officeNameFr : deselected.officeNameFr,
																officeNameNL : deselected.officeNameNL,
																officePersyId : deselected.officePersyId,
																officePriority :deselected.officePriority,
																officeRcCode : deselected.officeRcCode,
																officeType : deselected.officeType});
														}
													}else if(dragModel == "selectedOffices" && dropModel == "deselectedPUOffices"){
														$scope.selectedPUOffices = $.grep($scope.selectedOffices,function(element, index) {
																	return element.officeType == "P10";
																});
														var length = $scope.selectedPUOffices.length;
														console.log(length);
														var deselected = $scope.deselectedPUOffices[dropIndex];
														
														if(deselected.officeType == "P10" && length >= 7){
															$scope.deselectedPUOffices.splice(dropIndex, 1);
															$scope.deselectedPUOffices.splice(dropIndex, 0, {
																PUDescription : deselected.PUDescription,
																PUNumber : deselected.PUNumber,
																RId : deselected.RId,
																employeeId : deselected.employeeId,
																isDeleted : true,
																officeDistance : deselected.officeDistance,
																officeLanguage : deselected.officeLanguage,
																officeNameFr : deselected.officeNameFr,
																officeNameNL : deselected.officeNameNL,
																officePersyId : deselected.officePersyId,
																officePriority : 0,
																officeRcCode : deselected.officeRcCode,
																officeType : deselected.officeType});
														}else {
															$scope.deselectedPUOffices.splice(dropIndex, 1);
															$scope.selectedOffices.splice(dragIndex, 0, {
																PUDescription : deselected.PUDescription,
																PUNumber : deselected.PUNumber,
																RId : deselected.RId,
																employeeId : deselected.employeeId,
																isDeleted : deselected.isDeleted,
																officeDistance : deselected.officeDistance,
																officeLanguage : deselected.officeLanguage,
																officeNameFr : deselected.officeNameFr,
																officeNameNL : deselected.officeNameNL,
																officePersyId : deselected.officePersyId,
																officePriority :deselected.officePriority,
																officeRcCode : deselected.officeRcCode,
																officeType : deselected.officeType});
														}
													}else if(dragModel == "deselectedHomeOffices" && dropModel == "selectedOffices"){
														var deselected = $scope.selectedOffices[dropIndex];
															$scope.selectedOffices.splice(dropIndex, 1);
															$scope.selectedOffices.splice(dropIndex, 0, {
																PUDescription : deselected.PUDescription,
																PUNumber : deselected.PUNumber,
																RId : deselected.RId,
																employeeId : deselected.employeeId,
																isDeleted : false,
																officeDistance : deselected.officeDistance,
																officeLanguage : deselected.officeLanguage,
																officeNameFr : deselected.officeNameFr,
																officeNameNL : deselected.officeNameNL,
																officePersyId : deselected.officePersyId,
																officePriority : 0,
																officeRcCode : deselected.officeRcCode,
																officeType : deselected.officeType});
														
													}else if(dragModel == "deselectedPUOffices" && dropModel == "selectedOffices"){
														var deselected = $scope.selectedOffices[dropIndex];
															$scope.selectedOffices.splice(dropIndex, 1);
															$scope.selectedOffices.splice(dropIndex, 0, {
																PUDescription : deselected.PUDescription,
																PUNumber : deselected.PUNumber,
																RId : deselected.RId,
																employeeId : deselected.employeeId,
																isDeleted : false,
																officeDistance : deselected.officeDistance,
																officeLanguage : deselected.officeLanguage,
																officeNameFr : deselected.officeNameFr,
																officeNameNL : deselected.officeNameNL,
																officePersyId : deselected.officePersyId,
																officePriority : 0,
																officeRcCode : deselected.officeRcCode,
																officeType : deselected.officeType});
													}else if(dragModel == "deselectedHomeOffices" && dropModel == "deselectedPUOffices"){
														var deselected = $scope.deselectedPUOffices[dropIndex];
														$scope.deselectedPUOffices.splice(dropIndex, 1);
														$scope.deselectedHomeOffices.splice(dragIndex, 0, {
															PUDescription : deselected.PUDescription,
															PUNumber : deselected.PUNumber,
															RId : deselected.RId,
															employeeId : deselected.employeeId,
															isDeleted : deselected.isDeleted,
															officeDistance : deselected.officeDistance,
															officeLanguage : deselected.officeLanguage,
															officeNameFr : deselected.officeNameFr,
															officeNameNL : deselected.officeNameNL,
															officePersyId : deselected.officePersyId,
															officePriority :deselected.officePriority,
															officeRcCode : deselected.officeRcCode,
															officeType : deselected.officeType});
													}else if(dragModel == "deselectedPUOffices" && dropModel == "deselectedHomeOffices"){
														var deselected = $scope.deselectedHomeOffices[dropIndex];
														$scope.deselectedHomeOffices.splice(dropIndex, 1);
														$scope.deselectedPUOffices.splice(dragIndex, 0, {
															PUDescription : deselected.PUDescription,
															PUNumber : deselected.PUNumber,
															RId : deselected.RId,
															employeeId : deselected.employeeId,
															isDeleted : deselected.isDeleted,
															officeDistance : deselected.officeDistance,
															officeLanguage : deselected.officeLanguage,
															officeNameFr : deselected.officeNameFr,
															officeNameNL : deselected.officeNameNL,
															officePersyId : deselected.officePersyId,
															officePriority :deselected.officePriority,
															officeRcCode : deselected.officeRcCode,
															officeType : deselected.officeType});
													}
												}
											};
											$scope.officeUp = function(index) {
												if (index <= 0 || index >= $scope.selectedOffices.length)
													return;
												var temp = $scope.selectedOffices[index];
												$scope.selectedOffices[index] = $scope.selectedOffices[index - 1];
												$scope.selectedOffices[index - 1] = temp;
												
											};
											$scope.officeDown = function(index) {
												if (index < 0 || index >= ($scope.selectedOffices.length - 1))
													return;
												var temp = $scope.selectedOffices[index];
												$scope.selectedOffices[index] = $scope.selectedOffices[index + 1];
												$scope.selectedOffices[index + 1] = temp;
												
											};
											$scope.deleteSelectedOffice = function(index) {
												var temp = $scope.selectedOffices[index];
												if (temp.officeType == "H30") {
												$scope.deselectedHomeOffices.push({
													PUDescription : temp.PUDescription,
													PUNumber : temp.PUNumber,
													RId : temp.RId,
													employeeId : temp.employeeId,
													isDeleted : true,
													officeDistance : temp.officeDistance,
													officeLanguage : temp.officeLanguage,
													officeNameFr : temp.officeNameFr,
													officeNameNL : temp.officeNameNL,
													officePersyId : temp.officePersyId,
													officePriority : 0,
													officeRcCode : temp.officeRcCode,
													officeType : temp.officeType,
												});
												$scope.selectedOffices.splice(index, 1);
												}else if(temp.officeType == "P10"){
													$scope.deselectedPUOffices.push({
														PUDescription : temp.PUDescription,
														PUNumber : temp.PUNumber,
														RId : temp.RId,
														employeeId : temp.employeeId,
														isDeleted : true,
														officeDistance : temp.officeDistance,
														officeLanguage : temp.officeLanguage,
														officeNameFr : temp.officeNameFr,
														officeNameNL : temp.officeNameNL,
														officePersyId : temp.officePersyId,
														officePriority : 0,
														officeRcCode : temp.officeRcCode,
														officeType : temp.officeType,
													});
													$scope.selectedOffices.splice(index, 1);
												}else if(temp.officeType == "LAN"){
													vm.deselectedLANOffices.push({
														PUDescription : temp.PUDescription,
														PUNumber : temp.PUNumber,
														RId : temp.RId,
														employeeId : temp.employeeId,
														isDeleted : true,
														officeDistance : temp.officeDistance,
														officeLanguage : temp.officeLanguage,
														officeNameFr : temp.officeNameFr,
														officeNameNL : temp.officeNameNL,
														officePersyId : temp.officePersyId,
														officePriority : 0,
														officeRcCode : temp.officeRcCode,
														officeType : temp.officeType,
													});
													$scope.selectedOffices.splice(index, 1);
												}
											};
											$scope.addHomeOffice = function(index) {
												var temp = $scope.deselectedHomeOffices[index];
												$scope.selectedOffices.push({
													PUDescription : temp.PUDescription,
													PUNumber : temp.PUNumber,
													RId : temp.RId,
													employeeId : temp.employeeId,
													isDeleted : false,
													officeDistance : temp.officeDistance,
													officeLanguage : temp.officeLanguage,
													officeNameFr : temp.officeNameFr,
													officeNameNL : temp.officeNameNL,
													officePersyId : temp.officePersyId,
													officePriority : $scope.selectedOffices.length + 1,
													officeRcCode : temp.officeRcCode,
													officeType : temp.officeType,
												});
												$scope.deselectedHomeOffices.splice(index, 1);
											};
											$scope.addPUOffice = function(index) {
												var temp = $scope.deselectedPUOffices[index];
												$scope.selectedOffices.push({
													PUDescription : temp.PUDescription,
													PUNumber : temp.PUNumber,
													RId : temp.RId,
													employeeId : temp.employeeId,
													isDeleted : false,
													officeDistance : temp.officeDistance,
													officeLanguage : temp.officeLanguage,
													officeNameFr : temp.officeNameFr,
													officeNameNL : temp.officeNameNL,
													officePersyId : temp.officePersyId,
													officePriority : $scope.selectedOffices.length + 1,
													officeRcCode : temp.officeRcCode,
													officeType : temp.officeType,
												});
												$scope.deselectedPUOffices.splice(index, 1);
											};
											$scope.addLANOffice = function() {
												$scope.selectedOffices.push({
													PUDescription : vm.lanOffice.selected.PUDescription,
													PUNumber : vm.lanOffice.selected.PUNumber,
													RId : vm.lanOffice.selected.RId,
													employeeId : vm.lanOffice.selected.employeeId,
													isDeleted : false,
													officeDistance : vm.lanOffice.selected.officeDistance,
													officeLanguage : vm.lanOffice.selected.officeLanguage,
													officeNameFr : vm.lanOffice.selected.officeNameFr,
													officeNameNL : vm.lanOffice.selected.officeNameNL,
													officePersyId : vm.lanOffice.selected.officePersyId,
													officePriority : $scope.selectedOffices.length + 1,
													officeRcCode : vm.lanOffice.selected.officeRcCode,
													officeType : vm.lanOffice.selected.officeType,
												});
												for(var i = vm.deselectedLANOffices.length - 1; i >= 0; i--){
												    if(vm.deselectedLANOffices[i].RId == vm.lanOffice.selected.RId){
												    	vm.deselectedLANOffices.splice(i,1);
												    }
												}
												vm.clear();
											};
											$scope.saveList = function(){
												var modalInstance = $uibModal.open({
												      animation: true,
												      ariaLabelledBy: 'modal-title',
												      ariaDescribedBy: 'modal-body',
												      templateUrl: 'saveList.html',
												      controller: 'ModalInstanceCtrlSave',
												      controllerAs: '$savectrl',
												      scope : $scope
												    });
												    modalInstance.result.then(function () {
												    	$scope.confirnList();
												    }, function () {
												   
												    });
												};
											$scope.returnMessage = function(message){
												$scope.message = message;
												var modalInstance = $uibModal.open({
												      animation: true,
												      ariaLabelledBy: 'modal-title',
												      ariaDescribedBy: 'modal-body',
												      templateUrl: 'returnMessage.html',
												      controller: 'ModalInstanceCtrlReturn',
												      controllerAs: '$returnctrl',
												      scope : $scope
												    });
												    modalInstance.result.then(function () {
												    	
												    }, function () {
												   
												    });
												};
											$scope.confirnList = function() {
												
												$scope.allOffices = [];
												
												for (var i=0; i <= $scope.selectedOffices.length - 1; i++) {
													var temp = $scope.selectedOffices[i];
													$scope.allOffices.push({
														PUDescription : temp.PUDescription,
														PUNumber : temp.PUNumber,
														RId : temp.RId,
														employeeId : temp.employeeId,
														isDeleted : temp.isDeleted,
														officeDistance : temp.officeDistance,
														officeLanguage : temp.officeLanguage,
														officeNameFr : temp.officeNameFr,
														officeNameNL : temp.officeNameNL,
														officePersyId : temp.officePersyId,
														officePriority : i + 1,
														officeRcCode : temp.officeRcCode,
														officeType : temp.officeType,
													});
												    }
												
												for (var i=0; i <= $scope.deselectedHomeOffices.length - 1; i++) {
													var temp = $scope.deselectedHomeOffices[i];
													$scope.allOffices.push({
														PUDescription : temp.PUDescription,
														PUNumber : temp.PUNumber,
														RId : temp.RId,
														employeeId : temp.employeeId,
														isDeleted : temp.isDeleted,
														officeDistance : temp.officeDistance,
														officeLanguage : temp.officeLanguage,
														officeNameFr : temp.officeNameFr,
														officeNameNL : temp.officeNameNL,
														officePersyId : temp.officePersyId,
														officePriority : temp.officePriority,
														officeRcCode : temp.officeRcCode,
														officeType : temp.officeType,
													});
												    }
												
												for (var i=0; i <= $scope.deselectedPUOffices.length - 1; i++) {
													var temp = $scope.deselectedPUOffices[i];
													$scope.allOffices.push({
														PUDescription : temp.PUDescription,
														PUNumber : temp.PUNumber,
														RId : temp.RId,
														employeeId : temp.employeeId,
														isDeleted : temp.isDeleted,
														officeDistance : temp.officeDistance,
														officeLanguage : temp.officeLanguage,
														officeNameFr : temp.officeNameFr,
														officeNameNL : temp.officeNameNL,
														officePersyId : temp.officePersyId,
														officePriority : temp.officePriority,
														officeRcCode : temp.officeRcCode,
														officeType : temp.officeType,
													});
												    }
												
												for (var i=0; i <= vm.deselectedLANOffices.length - 1; i++) {
													var temp = vm.deselectedLANOffices[i];
													$scope.allOffices.push({
														PUDescription : temp.PUDescription,
														PUNumber : temp.PUNumber,
														RId : temp.RId,
														employeeId : temp.employeeId,
														isDeleted : temp.isDeleted,
														officeDistance : temp.officeDistance,
														officeLanguage : temp.officeLanguage,
														officeNameFr : temp.officeNameFr,
														officeNameNL : temp.officeNameNL,
														officePersyId : temp.officePersyId,
														officePriority : temp.officePriority,
														officeRcCode : temp.officeRcCode,
														officeType : temp.officeType,
													});
												    }
												        $('.loader').show();
														$.post(sendUrl.toString(), {
															<portlet:namespace />data : angular.toJson($scope.allOffices),
															<portlet:namespace />lang : $scope.lang
														}, function(data, status) {
															console.log((JSON.parse(data)).message);
															$('.loader').hide();
															$scope.returnMessage((JSON.parse(data)).message);
														});
													
												}
										});

							} ]);
	app.controller('ModalInstanceCtrlSave',['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
		  var $savectrl = this;
		  $savectrl.save = function () {		
		    $uibModalInstance.close();
		  };
		  $savectrl.lang = $scope.lang;
		  $savectrl.cancel = function () {
		    $uibModalInstance.dismiss('cancel');
		  };
		}]);
	app.controller('ModalInstanceCtrlReturn',['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
		  var $returnctrl = this;
		  $returnctrl.lang = $scope.lang;
		  $returnctrl.message = $scope.message;
		  $returnctrl.cancel = function () {
		    $uibModalInstance.dismiss('cancel');
		    window.location.href = "/group/redirect";
		  };
		}]);
	app.controller('ModalInstanceCtrlBegin',['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
		  var $beginctrl = this;
		  $beginctrl.lang = $scope.lang;
		  $beginctrl.beginMessage = $scope.beginMessage;
		  $beginctrl.cancel = function () {
		    $uibModalInstance.dismiss('cancel');
		    window.location.href = "/group/redirect";
		  };
		}]);
</script>
<div ng-app="myApp" class="ng-cloak" ng-controller="DemoCtrl as ctrl">
	<script type="text/ng-template" id="beginningMessage.html">
        <div class="modal-body" id="modal-body">
            {{$beginctrl.beginMessage}}
        </div>
        <div class="modal-footer">
            <button class="btn btn-warning" type="button" ng-click="$beginctrl.cancel()">Ok</button>
        </div>
        
    </script>
	<script type="text/ng-template" id="returnMessage.html">
        <div class="modal-body" id="modal-body">
            {{$returnctrl.message}}
        </div>
        <div class="modal-footer">
            <button class="btn btn-warning" type="button" ng-click="$returnctrl.cancel()">Ok</button>
        </div>
    </script>
	<script type="text/ng-template" id="saveList.html">
        <div class="modal-body" id="modal-body" ng-if="lang == 'fr_FR'">
           Etes-vous certain de vouloir valider votre liste de choix ?
        </div>
        <div class="modal-body" id="modal-body" ng-if="lang != 'fr_FR'">
           Weet je zeker dat je je keuzelijst wilt bevestigen?
        </div>
        <div class="modal-footer" ng-if="lang == 'fr_FR'" >
            <button class="btn btn-primary" type="button" ng-click="$savectrl.save()">Oui</button>
            <button class="btn btn-warning" type="button" ng-click="$savectrl.cancel()">Non</button>
        </div>
        <div class="modal-footer" ng-if="lang != 'fr_FR'" >
            <button class="btn btn-primary" type="button" ng-click="$savectrl.save()">JA</button>
            <button class="btn btn-warning" type="button" ng-click="$savectrl.cancel()">NEE</button>
        </div>
    </script>
	<div class="container" ng-if="beginMessage == ''">
		<div ng-if="isAllowed == true">
			<div class="row">
				<div class="col-md-12">
					<h2>{{name}} ({{id}}) - {{pUnit}}</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading" ng-if="lang == 'fr_FR'">Ma liste
							des bureaux ({{selectedOffices.length}} bureaux):</div>
						<div class="panel-heading" ng-if="lang != 'fr_FR'">Mijn
							kantorenlijst ({{selectedOffices.length}} kantoren):</div>
						<div class="panel-body selected-office" ng-if="lang == 'fr_FR'">

							<div ui-sortable="sortableOptions" ng-model="selectedOffices"
								class="list">
								<div ng-repeat="office in selectedOffices" class="office"
									ng-class="{'background-yellow' : office.officeType == 'P10','background-blue' : office.officeType == 'H30','background-green' : office.officeType =='PUO','background-red' : office.officeType =='LAN'}"
									ng-init="office.officePriority=$index + 1">
									{{office.officeNameFr}}({{office.officeRcCode}}) -
									{{office.officeDistance}}km &nbsp;<span class="buttons"><i
										ng-if="$index > 0" ng-click="officeUp($index)"
										class="fa fa-chevron-circle-up" aria-hidden="true"></i> <i
										ng-if="(selectedOffices).length-1 != $index"
										ng-click="officeDown($index)"
										class="fa fa-chevron-circle-down" aria-hidden="true"></i> <i
										ng-if="((selectedOffices | filter : 'P10').length > 7 || office.officeType != 'P10') && office.officeType != 'PUO'"
										ng-click="deleteSelectedOffice($index)" class="fa fa-times"
										aria-hidden="true"></i></span>
								</div>
							</div>
						</div>
						<div class="panel-body selected-office" ng-if="lang != 'fr_FR'">

							<div ui-sortable="sortableOptions" ng-model="selectedOffices"
								class="list">
								<div ng-repeat="office in selectedOffices" class="office"
									ng-class="{'background-yellow' : office.officeType == 'P10','background-blue' : office.officeType == 'H30','background-green' : office.officeType =='PUO','background-red' : office.officeType =='LAN'}"
									ng-init="office.officePriority=$index + 1">
									{{office.officeNameNL}}({{office.officeRcCode}}) -
									{{office.officeDistance}}km &nbsp;<span class="buttons"><i
										ng-if="$index > 0" ng-click="officeUp($index)"
										class="fa fa-chevron-circle-up" aria-hidden="true"></i> <i
										ng-if="(selectedOffices).length-1 != $index"
										ng-click="officeDown($index)"
										class="fa fa-chevron-circle-down" aria-hidden="true"></i> <i
										ng-if="((selectedOffices | filter : 'P10').length > 7 || office.officeType != 'P10') && office.officeType != 'PUO'"
										ng-click="deleteSelectedOffice($index)" class="fa fa-times"
										aria-hidden="true"></i></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-lg-6">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading" ng-if="lang == 'fr_FR'">Adresse
									:</div>
								<div class="panel-heading" ng-if="lang != 'fr_FR'">Adres :</div>
								<div class="panel-body">
									<p style="margin-top: 15px;">{{homeAddress}}</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading" ng-if="lang == 'fr_FR'">Ajouter
									un bureau :</div>
								<div class="panel-heading" ng-if="lang != 'fr_FR'">Voeg
									een kantoor toe :</div>
								<div class="panel-body" ng-if="lang == 'fr_FR'"
									style="padding: 15px 0 15px 15px;display: flex;">
									<ui-select ng-model="ctrl.lanOffice.selected" theme="bootstrap"
										ng-disabled="ctrl.disabled" reset-search-input="false"
										style="width: 80%;float:left;" title="Choose an office">
									<ui-select-match placeholder="Ajouter un bureau...">{{$select.selected.officeNameFr}}</ui-select-match>
									<ui-select-choices
										repeat="lanOffice in ctrl.deselectedLANOffices | propsFilter: {officeNameFr: $select.search, officeRcCode: $select.search}"
										refresh-delay="0">
									<div
										ng-bind-html="lanOffice.officeNameFr | highlight: $select.search"></div>
									<small
										ng-bind-html="lanOffice.officeRcCode | highlight: $select.search"></small>
									</ui-select-choices> </ui-select>
									<span style="float: left;"><i ng-click="addLANOffice()"
										class="fa fa-plus-circle" aria-hidden="true"
										style="font-size: 2.5em; margin: 0px 0px 0px 5px;"></i></span>
								</div>
								<div class="panel-body" ng-if="lang != 'fr_FR'"
									style="padding: 15px 0 15px 15px;display: flex;">
									<ui-select ng-model="ctrl.lanOffice.selected" theme="bootstrap"
										ng-disabled="ctrl.disabled" reset-search-input="false"
										style="width: 80%;float:left;" title="Choose an office">
									<ui-select-match placeholder="Voeg een kantoor toe...">{{$select.selected.officeNameNL}}</ui-select-match>
									<ui-select-choices
										repeat="lanOffice in ctrl.deselectedLANOffices | propsFilter: {officeNameNL: $select.search, officeRcCode: $select.search}"
										refresh-delay="0">
									<div
										ng-bind-html="lanOffice.officeNameNL | highlight: $select.search"></div>
									<small
										ng-bind-html="lanOffice.officeRcCode | highlight: $select.search"></small>
									</ui-select-choices> </ui-select>
									<span style="float: left;"><i ng-click="addLANOffice()"
										class="fa fa-plus-circle" aria-hidden="true"
										style="font-size: 2.5em; margin: 0px 0px 0px 5px;"></i></span>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading" ng-if="lang == 'fr_FR'">Bureaux
									supprimés dans un rayon de 30km autour de mon domicile
									({{deselectedHomeOffices.length}} bureaux):</div>
								<div class="panel-heading" ng-if="lang != 'fr_FR'">Verwijderde
									kantoren binnen de 30km van mijn adres
									({{deselectedHomeOffices.length}} kantoren):</div>
								<div class="panel-body home-office" ng-if="lang == 'fr_FR'">
									<div ui-sortable="sortableOptions"
										ng-model="deselectedHomeOffices" class="list">
										<div ng-repeat="office in deselectedHomeOffices"
											class="office"
											ng-class="{'background-blue' : office.officeType == 'H30'}">
											{{office.officeNameFr}}({{office.officeRcCode}}) -
											{{office.officeDistance}}km &nbsp;<span
												ng-click="addHomeOffice($index)" class="buttons"><i
												class="fa fa-plus-circle" aria-hidden="true"></i></span>
										</div>
									</div>
								</div>
								<div class="panel-body home-office" ng-if="lang != 'fr_FR'">
									<div ui-sortable="sortableOptions"
										ng-model="deselectedHomeOffices" class="list">
										<div ng-repeat="office in deselectedHomeOffices"
											class="office"
											ng-class="{'background-blue' : office.officeType == 'H30'}">
											{{office.officeNameNL}}({{office.officeRcCode}}) -
											{{office.officeDistance}}km &nbsp;<span
												ng-click="addHomeOffice($index)" class="buttons"><i
												class="fa fa-plus-circle" aria-hidden="true"></i></span>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading" ng-if="lang == 'fr_FR'">Bureaux
									supprimés dans un rayon de 10km autour de ma Planning Unit
									({{deselectedPUOffices.length}} bureaux):</div>
								<div class="panel-heading" ng-if="lang != 'fr_FR'">Verwijderde
									kantoren binnen de 10km van mijn PU
									({{deselectedPUOffices.length}} kantoren):</div>
								<div class="panel-body pu-office" ng-if="lang == 'fr_FR'">
									<div ui-sortable="sortableOptions"
										ng-model="deselectedPUOffices" class="list">
										<div ng-repeat="office in deselectedPUOffices" class="office"
											ng-class="{'background-yellow' : office.officeType == 'P10'}">
											{{office.officeNameFr}}({{office.officeRcCode}}) -
											{{office.officeDistance}}km &nbsp;<span
												ng-click="addPUOffice($index)" class="buttons"><i
												class="fa fa-plus-circle" aria-hidden="true"></i></span>
										</div>
									</div>
								</div>
								<div class="panel-body pu-office" ng-if="lang != 'fr_FR'">
									<div ui-sortable="sortableOptions"
										ng-model="deselectedPUOffices" class="list">
										<div ng-repeat="office in deselectedPUOffices" class="office"
											ng-class="{'background-yellow' : office.officeType == 'P10'}">
											{{office.officeNameNL}}({{office.officeRcCode}}) -
											{{office.officeDistance}}km &nbsp;<span
												ng-click="addPUOffice($index)" class="buttons"><i
												class="fa fa-plus-circle" aria-hidden="true"></i></span>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading" ng-if="lang == 'fr_FR'"
									style="border-bottom: none; font-size: 17px;">
									<!--<p>
										<i class="fa fa-info-circle" aria-hidden="true"
											style="margin-right: 5px;"></i>Pour plus d’informations : <a
											style="color: blue;"
											href="https://www.bpost4me.be/documents/11050273/22115953/FR_Titel_Handleiding_IPZ.pdf">manuel</a>,
										<a style="color: blue;"
											href="https://www.bpost4me.be/documents/11050273/22115953/02_Zones_de_planification_ind_20180117_FR.pdf">présentation</a>
									</p>-->
									<p>
										<i class="fa fa-info-circle" aria-hidden="true"
											style="margin-right: 5px;"></i>Un collaborateur de l’équipe
										d’intervention doit obligatoirement conserver un minimum de 7
										bureaux dans un rayon de 10km autour de son unité de
										planification (orange)
									</p>
								</div>
								<div class="panel-heading" ng-if="lang != 'fr_FR'"
									style="border-bottom: none; font-size: 17px;">
									<!--<p>
										<i class="fa fa-info-circle" aria-hidden="true"
											style="margin-right: 5px;"></i>Voor meer informatie: <a
											style="color: blue;"
											href="https://www.bpost4me.be/documents/11050273/22115953/NL_Titel_Handleiding_IPZ.pdf">handleiding</a>,
										<a style="color: blue;"
											href="https://www.bpost4me.be/documents/11050273/22115953/02_Individuele_planning_zones_20180122+NL.pdf">presentatie</a>
									</p>-->
									<p>
										<i class="fa fa-info-circle" aria-hidden="true"
											style="margin-right: 5px;"></i>Een medewerker van een
										interventieteam dient minimum 7 kantoren binnen een straal van
										10km rond zijn planning unit grens te behouden(oranje)
									</p>
								</div>

							</div>
						</div>
					</div>
					<button ng-click="saveList()" class="btn btn-primary sub-button"
						ng-if="lang != 'fr_FR'">Ik bevestig mijn keuzelijst</button>
					<button ng-click="saveList()" class="btn btn-primary sub-button"
						ng-if="lang == 'fr_FR'">Je confirme ma liste de choix</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="loader">
	<img class="loading"
		src="<%=request.getContextPath()%>/images/loader2.gif" />
</div>